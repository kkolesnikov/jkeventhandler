﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Jk.CodeGeneration;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace test.Jk.EventHandler.CommonGeneration
{
    [TestClass]
    public class ConcreteProxyGeneratorTests
    {
        [TestMethod]
        public async Task Test()
        {
            var referenceResolverMock = new Mock<IReferenceResolver>();
            var nameGeneratorMock = new Mock<INameGenerator>();
            nameGeneratorMock.Setup(a => a.ResolveAssemblyName()).Returns("TestAssembly");
            referenceResolverMock.Setup(a => a.GenerateReferences(It.IsAny<Type[]>())).Returns((PortableExecutableReference[]) new MetadataReference[]
            {
                MetadataReference.CreateFromFile(typeof (object).Assembly.Location),
                MetadataReference.CreateFromFile(typeof (Enumerable).Assembly.Location)
            });
            var generator = new TestConcreteProxyGenerator(null, referenceResolverMock.Object, null,
                nameGeneratorMock.Object);
            var result = await generator.Process(new Type[0], new Type[0]);
        }
    }

    public class TestConcreteProxyGenerator : ConcreteProxyGenerator
    {
        public TestConcreteProxyGenerator(INamespaceGenerator namespaceGenerator, IReferenceResolver referenceResolver,
            IUsingsGenerator usingsGenerator, INameGenerator nameGenerator)
            : base(namespaceGenerator, referenceResolver, usingsGenerator, nameGenerator)
        {
        }

        protected override CompilationUnitSyntax GenerateCompilationUnit(Type[] types, Type[] implementations)
        {
            var code = @"
    using System;

    namespace RoslynCompileSample
    {
        public class Writer
        {
            public void Write(string message)
            {
                Console.WriteLine(message);
            }
        }
    }";
            return SyntaxFactory.ParseCompilationUnit(code);
        }
    }
}