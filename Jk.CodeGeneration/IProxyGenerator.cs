﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;

namespace Jk.CodeGeneration
{
    public interface IProxyGenerator
    {
        Task<Assembly> Process(Type[] types, Type[] implementations);
    }
}