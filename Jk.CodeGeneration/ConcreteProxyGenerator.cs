using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Emit;

namespace Jk.CodeGeneration
{
    public class ConcreteProxyGenerator: IProxyGenerator
    {
        private INamespaceGenerator _namespaceGenerator;
        private IReferenceResolver _referenceResolver;
        private IUsingsGenerator _usingsGenerator;
        private INameGenerator _nameGenerator;
        public ConcreteProxyGenerator(INamespaceGenerator namespaceGenerator, IReferenceResolver referenceResolver, IUsingsGenerator usingsGenerator, INameGenerator nameGenerator)
        {
            _namespaceGenerator = namespaceGenerator;
            _referenceResolver = referenceResolver;
            _usingsGenerator = usingsGenerator;
            _nameGenerator = nameGenerator;
        }

        protected virtual CompilationUnitSyntax GenerateCompilationUnit(Type[] types, Type[] implementationTypes)
        {
            var nameSpace = _namespaceGenerator.GenerateNamespace(types, implementationTypes);
            var usings = _usingsGenerator.GenerateUsings(types);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .WithUsings(SyntaxFactory.List<UsingDirectiveSyntax>(usings))
                .WithMembers(SyntaxFactory.List<MemberDeclarationSyntax>(new[] {nameSpace}));
            return compilationUnit;
        }

        public async Task<Assembly> Process(Type[] types, Type[] implementations)
        {
            var compilationUnit = GenerateCompilationUnit(types, implementations);
            var references = _referenceResolver.GenerateReferences(types);
            //  var tree = CSharpSyntaxTree.Create(compilationUnit);
            var str = compilationUnit.NormalizeWhitespace().ToString();
            var renewedTree = CSharpSyntaxTree.ParseText(str);
            var compilation = CSharpCompilation.Create(_nameGenerator.ResolveAssemblyName(), new[] { renewedTree }, references,new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
            return await Task.FromResult(Compile(compilation));
        }

        protected Assembly Compile(CSharpCompilation compilation)
        {
            using (var ms = new MemoryStream())
            {
                EmitResult result = compilation.Emit(ms);

                if (!result.Success)
                {
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    foreach (Diagnostic diagnostic in failures)
                    {
                        Console.Error.WriteLine("{0}: {1}", diagnostic.Id, diagnostic.GetMessage());
                    
                    }
                }
                else
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    Assembly assembly = Assembly.Load(ms.ToArray());
                    return assembly;
                }
                return null;
            }
        }
    }
}