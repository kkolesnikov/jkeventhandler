﻿using System.Security.Cryptography.X509Certificates;

namespace Jk.CodeGeneration
{
    public interface INameGenerator
    {
        string ResolveAssemblyName();

    }
}