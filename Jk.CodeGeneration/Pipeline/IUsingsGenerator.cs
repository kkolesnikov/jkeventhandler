using System;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Jk.CodeGeneration
{
    public interface IUsingsGenerator
    {
        UsingDirectiveSyntax[] GenerateUsings(Type[] types);
    }
}