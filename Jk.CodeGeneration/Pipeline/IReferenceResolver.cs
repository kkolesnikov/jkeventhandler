using System;
using Microsoft.CodeAnalysis;

namespace Jk.CodeGeneration
{
    public interface IReferenceResolver
    {
        PortableExecutableReference[] GenerateReferences(Type[] types);
    }
}