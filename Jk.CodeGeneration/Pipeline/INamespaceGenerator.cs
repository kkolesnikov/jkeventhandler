﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Jk.CodeGeneration
{
    public interface INamespaceGenerator
    {
        NamespaceDeclarationSyntax GenerateNamespace(Type[] interfacesTypes, Type[] implementationsTypes);
    }
}
