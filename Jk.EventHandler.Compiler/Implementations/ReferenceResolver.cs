using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jk.CodeGeneration;
using Microsoft.CodeAnalysis;

namespace Jk.EventHandler.Compiler.Implementations
{
    public class ReferenceResolver : IReferenceResolver
    {
        public PortableExecutableReference[] GenerateReferences(Type[] types)
        {
            HashSet<string> referencesLocations = new HashSet<string>();
            foreach (var type in types)
            {
                referencesLocations.Add(type.Assembly.Location);
            }
            referencesLocations.Add(typeof (Task).Assembly.Location);
            referencesLocations.Add(typeof (BaseProxyEventHandler<>).Assembly.Location);
            referencesLocations.Add(typeof (NamespaceGenerator).Assembly.Location);
            return referencesLocations.Select(a => MetadataReference.CreateFromFile(a)).ToArray();
        }
    }
}