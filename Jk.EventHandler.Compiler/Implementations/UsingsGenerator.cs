using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jk.CodeGeneration;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Jk.EventHandler.Compiler.Implementations
{
    public class UsingsGenerator : IUsingsGenerator {
        public UsingDirectiveSyntax[] GenerateUsings(Type[] types)
        {
            HashSet<string> namespaces = new HashSet<string>();
            foreach (var type in types)
            {
                namespaces.Add(type.Namespace);
                var constructors = type.GetConstructors();
                foreach (var parameterInfo in constructors.SelectMany(a=>a.GetParameters()))
                {
                    namespaces.Add(parameterInfo.ParameterType.Namespace);
                }
            }
            namespaces.Add(typeof (Task).Namespace);
            namespaces.Add(typeof (object).Namespace);
            namespaces.Add(typeof (BaseProxyEventHandler<>).Namespace);
            namespaces.Add(typeof (List<>).Namespace);
            return namespaces.Select(a => SyntaxFactory.UsingDirective(SyntaxFactory.ParseName(a))).ToArray();
        }

       
    }
}