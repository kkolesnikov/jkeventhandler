﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jk.CodeGeneration;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Jk.EventHandler.Compiler.Implementations
{
    public class NamespaceGenerator: INamespaceGenerator
    {
        public NamespaceDeclarationSyntax GenerateNamespace(Type[] interfacesTypes, Type[] implementationsTypes)
        {
            List<ClassDeclarationSyntax> classes = new List<ClassDeclarationSyntax>();
            foreach (var type in interfacesTypes)
            {
                var className = GenerateClassName(type);
                var typeClass = SyntaxFactory.ClassDeclaration(className)
                    .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)))
                    .WithBaseList(
                        SyntaxFactory.BaseList(
                            SyntaxFactory.SeparatedList<BaseTypeSyntax>(new[]
                            { SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName($"BaseProxyEventHandler<{type.Name}>")),SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName(type.Name))})));
                List<MemberDeclarationSyntax> memberDeclarationSyntaxs = new List<MemberDeclarationSyntax>();
                var constructor = CreateConstructor(className, implementationsTypes.Where(a => type.IsAssignableFrom(a)).ToArray());
                memberDeclarationSyntaxs.Add(constructor);
                foreach (var reflectionMethod in type.GetMethods())
                {
                    var relfectioinParameters = reflectionMethod.GetParameters();
                    var parametersString = "(" +
                        relfectioinParameters
                            .Select(a => a.ParameterType.Name + " " + a.Name)
                            .Aggregate((a, b) => a + ',' + b) + ")";
                    var argumentsStirng = "(" + reflectionMethod.GetParameters().Select(a => a.Name).Aggregate((a,b)=>a+","+b) + ")";
                    var methodSyntax =
                        SyntaxFactory.MethodDeclaration(SyntaxFactory.ParseTypeName("Task"), reflectionMethod.Name)
                            .WithParameterList(SyntaxFactory.ParseParameterList(parametersString))
                            .WithBody(
                                SyntaxFactory.Block(
                                    SyntaxFactory.ParseStatement(
                                        $"return Process((h) => h.{reflectionMethod.Name}{argumentsStirng});"))).WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.ParseToken("public")));
                    memberDeclarationSyntaxs.Add(methodSyntax);
                }
                //var constructor =
                //    SyntaxFactory.ConstructorDeclaration(className)
                //        .WithParameterList(
                //            SyntaxFactory.ParseParameterList(
                //                "(ICollection<ISomeHandler> realHandlers): base(realHandlers)"))
                //        .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.ParseToken("public"))).WithBody(SyntaxFactory.Block());
                
                typeClass = typeClass.WithMembers(SyntaxFactory.List(memberDeclarationSyntaxs));
                classes.Add(typeClass);
            };
            var ns = SyntaxFactory.NamespaceDeclaration(SyntaxFactory.ParseName("Generated.Generated")).WithMembers(SyntaxFactory.List<MemberDeclarationSyntax>(classes));
            return ns;
        }

        private ConstructorDeclarationSyntax CreateConstructor(string className, Type[] implementations)
        {
            List<string> allArgumentsList = new List<string>();
            int counter = 0;
            List<StatementSyntax> statementSyntaxs = new List<StatementSyntax>();
            foreach (var implementation in implementations)
            {
                var constructorInfo = implementation.GetConstructors().FirstOrDefault();
                List<string> implementationParameters = new List<string>();
                foreach (var parameterInfo in constructorInfo.GetParameters())
                {
                    var argumentName = $"arg{counter}";
                    var argumentType = parameterInfo.ParameterType.Name;
                    allArgumentsList.Add($"{argumentType} {argumentName}");
                    implementationParameters.Add(argumentName);
                    counter++;
                }
                var statement = SyntaxFactory.ParseStatement(
                    $"AddFactoryMethod(()=>new {implementation.Name}({implementationParameters.Aggregate((a, b) => a + "," + b)}));");
                statementSyntaxs.Add(statement);
            }
            var constructor = SyntaxFactory.ConstructorDeclaration(className).WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.ParseToken("public"))).WithBody(SyntaxFactory.Block().WithStatements(SyntaxFactory.List(statementSyntaxs))).WithParameterList(SyntaxFactory.ParseParameterList($"({allArgumentsList.Aggregate((a,b)=>a+","+b)})"));
            return constructor;
        }

        public string GenerateClassName(Type interfaceType)
        {
            var guid = "g_"+Guid.NewGuid().ToString("d").Replace("-", "");
            return guid + interfaceType.Name.Remove(0, 1);
        }

    }
}
