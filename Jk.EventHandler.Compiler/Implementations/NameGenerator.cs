using Jk.CodeGeneration;

namespace Jk.EventHandler.Compiler.Implementations
{
    public class NameGenerator : INameGenerator
    {
        public string ResolveAssemblyName()
        {
            return "GeneratedEventHandlers";
        }
    }
}