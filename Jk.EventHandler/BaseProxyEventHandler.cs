﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jk.EventHandler
{
    public class BaseProxyEventHandler<TEventHandler> where TEventHandler : IEventHandler
    {
        protected List<TEventHandler> Handlers = new List<TEventHandler>();

        protected async Task Process(Func<TEventHandler, Task> func)
        {
            foreach (var someEventHandler in Handlers)
            {
                await func(someEventHandler);
            }
        }

        protected void AddFactoryMethod(Func<TEventHandler> factoryMethod)
        {
            Handlers.Add(factoryMethod());
        }
    }

}
