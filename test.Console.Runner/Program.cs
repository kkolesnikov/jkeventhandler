﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Jk.CodeGeneration;
using Jk.EventHandler.Compiler.Implementations;

namespace test.Console.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            Do().Wait();
            System.Console.ReadKey();
        }

        public static async Task Do()
        {
            ConcreteProxyGenerator generator = new ConcreteProxyGenerator(new NamespaceGenerator(),
                new ReferenceResolver(), new UsingsGenerator(), new NameGenerator());

            //Динамически компилим сборку. В качестве параметров передаем все интерфейсы хендлеров и все их типы реализаций. Эту сборку можно в дальнейшем закешировать и не компилировать
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var assembly = await generator.Process(new Type[] { typeof(ISomeEventHandler) }, new Type[] { typeof(SomeEventHandlerImpl1), typeof(SomeEventHandlerImpl2) });
            stopwatch.Stop();
            System.Console.WriteLine($"Compilation: {stopwatch.Elapsed.TotalMilliseconds} ms");
            ContainerBuilder builder = new ContainerBuilder();

                //Получаем тип из сгенерированной сборки и регистрируем его
                var type = assembly.GetTypes().FirstOrDefault();
                builder.RegisterType(type).As<ISomeEventHandler>();

                //Регистрируем зависимости
                builder.RegisterType<SomeSource>().As<ISomeSource>();
                builder.RegisterType<SomeAnotherSource>().As<ISomeAnotherSource>();

                var container = builder.Build();
                var someHandler = container.Resolve<ISomeEventHandler>();
                    
                //Console: "Printed something"
                await someHandler.DoSomething(new SomeClass1());



        }
    }
}
