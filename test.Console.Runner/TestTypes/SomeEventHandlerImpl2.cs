﻿using System.Threading.Tasks;

namespace test.Console.Runner
{
    public class SomeEventHandlerImpl2 : ISomeEventHandler
    {

        private ISomeAnotherSource _someAnotherSource;

        public SomeEventHandlerImpl2(ISomeAnotherSource someAnotherSource)
        {

            _someAnotherSource = someAnotherSource;
        }


        public Task DoSomething(SomeClass1 some)
        {
            _someAnotherSource.PrintSomething();
            return Task.FromResult(0);
        }
    }
}