﻿using System.Threading.Tasks;
using Jk.EventHandler;

namespace test.Console.Runner
{
    public interface ISomeEventHandler : IEventHandler
    {
        Task DoSomething(SomeClass1 some);
    }
}