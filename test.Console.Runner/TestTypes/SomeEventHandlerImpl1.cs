﻿using System.Threading.Tasks;

namespace test.Console.Runner
{
    public class SomeEventHandlerImpl1 : ISomeEventHandler
    {
        private ISomeSource _someSource;

        public SomeEventHandlerImpl1(ISomeSource someSource)
        {
            _someSource = someSource;
        }


        public Task DoSomething(SomeClass1 some)
        {
            _someSource.PrintSomething();
            return Task.FromResult(0);
        }
    }
}