﻿namespace test.Console.Runner
{
    public class SomeSource : ISomeSource
    {
        public void PrintSomething()
        {
            System.Console.WriteLine("Printed something");
        }
    }
}