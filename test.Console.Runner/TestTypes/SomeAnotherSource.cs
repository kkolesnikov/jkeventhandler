namespace test.Console.Runner
{
    public class SomeAnotherSource : ISomeAnotherSource
    {
        public void PrintSomething()
        {
            System.Console.WriteLine("Print something else");
        }
    }
}